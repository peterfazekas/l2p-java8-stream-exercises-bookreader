# l2p-java8-stream-exercises-bookreader

1. Create the following classes:  
<dl>
    <dt>Author with fields:</dt>
        <dd>String name</dd>
        <dd>short age</dd>
        <dd>List<Book> books</dd>
    <dt>Book with fields</dt>
        <dd>String title</dd>
        <dd>List<Author> authors</dd>
        <dd>int numberOfPages</dd>
</dl>
2. Create a stream of books and then:
3. Check if some/all book(s) have more than 200 pages;
4. Find the books with max and min number of pages;
5. Filter books with only single author; sort the books by number of pages/title; get list of all titles;
6. Print them using forEach method
7. Get distinct list of all authors
8. Use peek method for debugging intermediate streams during execution the previous task.
9. Use parallel stream with task #3.
10. Use the Optional type for determining the title of the biggest book of some author. 