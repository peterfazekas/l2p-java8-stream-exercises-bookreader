package com.epam.bookreader;

import com.epam.bookreader.stream.StreamExampleFacade;
import com.epam.bookreader.service.BookData;
import com.epam.bookreader.service.DataSource;

public class App {

    final StreamExampleFacade streamExample;

    public App() {
        BookData data = new BookData(DataSource.URL);
        streamExample = new StreamExampleFacade(data.getAuthors(), data.getBooks());
    }

    public static void main(String[] args) {
        App app = new App();
        app.print();
    }

    private void print() {
        System.out.println(" 1. DTO classes were created!");
        System.out.println(" 2. " + streamExample.getInitState());
        System.out.println(" 3. " + streamExample.getBookByPageNumber(200));
        System.out.println(" 4. " + streamExample.getLongestBook());
        System.out.println("    " + streamExample.getShortestBook());
        System.out.println(" 5. " + streamExample.getSingleAuthorBooks());
        System.out.println(" 7. " + streamExample.getAuthorsList());
        System.out.println(" 8. " + streamExample.debugAuthorsList());
        System.out.println(" 9. " + streamExample.getBookByPageNumberParallel(200));
        System.out.println(streamExample.getLongestBookByAuthor("10. Pls enter an author name") );
    }

}
