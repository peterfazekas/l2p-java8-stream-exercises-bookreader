package com.epam.bookreader.service;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.*;
import java.util.stream.Collectors;

class BookParser {


    private static final String AUTHOR_PREFIX = "by ";
    private static final String SEPARATOR = ", ";
    private static final String PAGES_PREFIX = "Pages: ";

    final private Set<Author> bookAuthors;
    final private List<Book> books;

    private String title;
    private List<Author> authors;
    private short pageNum;
    private String prevLine;

    BookParser() {
        bookAuthors = new TreeSet<>();
        books = new ArrayList<>();
    }

    void parse(final List<String> lines) {
        lines.forEach(this::parseLine);
    }

    private void parseLine(final String line) {
        if (line.startsWith(AUTHOR_PREFIX)) {
            title = prevLine;
            authors = parseAuthors(line.substring(AUTHOR_PREFIX.length()));
        }
        if (line.startsWith(PAGES_PREFIX)) {
            pageNum = toNumber(line);
        }
        if (valid()) {
            books.add(new Book(title, authors, pageNum));
            bookAuthors.addAll(authors);
            init();
        }
        prevLine = line;
    }

    private short toNumber(final String line) {
        return Short.parseShort(line.substring(PAGES_PREFIX.length()));
    }

    private void init() {
        title = "";
        authors = null;
        pageNum = 0;
    }

    private boolean valid() {
        return title != "" && authors != null && pageNum > 0;
    }

    private List<Author> parseAuthors(final String line) {
        String[] names = line.split(SEPARATOR);
        return Arrays.stream(names)
                .map(String::trim)
                .map(Author::new)
                .collect(Collectors.toList());
    }

    public List<Book> getBooks() {
        return books;
    }

    public Set<Author> getAuthors() {
        return bookAuthors;
    }
}
