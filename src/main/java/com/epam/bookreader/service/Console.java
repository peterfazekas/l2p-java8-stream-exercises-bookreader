package com.epam.bookreader.service;

import java.util.Scanner;

public class Console {

    private final Scanner scanner;

    public Console(final Scanner scanner) {
        this.scanner = scanner;
    }

    public String read(final String description) {
        System.out.print(description + ": ");
        return scanner.nextLine().toLowerCase().trim();
    }
}
