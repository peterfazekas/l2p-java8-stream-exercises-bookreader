package com.epam.bookreader.service;

public enum DataSource {

    FILE(new FileBookReader(), "books.txt"),
    URL(new UrlBookReader(), "http://www.cafeaulait.org/books.html");

    private final BookReader bookReader;
    private final String input;

    DataSource(BookReader bookReader, String input) {
        this.bookReader = bookReader;
        this.input = input;
    }

    public BookReader getBookReader() {
        return bookReader;
    }

    public String getInput() {
        return input;
    }
}
