package com.epam.bookreader.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UrlBookReader implements BookReader {

    private static final char HTML_OPEN_TAG = '<';
    private static final char HTML_CLOSE_TAG = '>';

    @Override
    public List<String> read(final String urlString) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(getUrl(urlString).openStream()))) {
            lines = br.lines().collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cleanText(lines);
    }

    private List<String> cleanText(final List<String> htmlPageLines) {
        return htmlPageLines.stream()
                .map(this::cleanLine)
                .map(String::trim)
                .collect(Collectors.toList());
    }

    private String cleanLine(final String line) {
        StringBuilder sb = new StringBuilder();
        boolean isHtmlTag = false;
        for (char ch : line.toCharArray()) {
            if (ch == HTML_OPEN_TAG) {
                isHtmlTag = true;
            }
            if (!isHtmlTag) {
                sb.append(ch);
            }
            if (ch == HTML_CLOSE_TAG) {
                isHtmlTag = false;
            }
        }
        return sb.toString();
    }

    private URL getUrl(final String urlString) {
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }
}
