package com.epam.bookreader.service;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BookData {

    final Set<Author> authors;
    final List<Book> books;

    public BookData(final DataSource dataSource) {
        BookReader file = dataSource.getBookReader();
        BookParser data = new BookParser();
        data.parse(file.read(dataSource.getInput()));
        authors = data.getAuthors();
        books = data.getBooks();
        setAuthorBooks();
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public List<Book> getBooks() {
        return books;
    }

    private void setAuthorBooks() {
        authors.forEach(author -> author.setBooks(getBooksByAuthor(author)));
    }

    private List<Book> getBooksByAuthor(final Author author) {
        return books.stream()
                .filter(book -> book.isAuthor(author))
                .collect(Collectors.toList());
    }

}
