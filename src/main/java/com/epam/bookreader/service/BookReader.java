package com.epam.bookreader.service;

import java.util.List;

public interface BookReader {
    List<String> read(String fileName);
}
