package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Task 4.: Find the books with certain number of pages;
 */
public abstract class AbstractBookByPageNameExample extends AbstractExample {

    private static final String DELIMITER = ", ";

    AbstractBookByPageNameExample(final Set<Author> authors, final List<Book> books) {
        super(authors, books);
    }

    protected abstract int getPageNumber();

    String getBooksByPageNumber() {
        return books.stream()
                .filter(byPageNumber())
                .map(Book::getTitle)
                .collect(Collectors.joining(DELIMITER));
    }

    private Predicate<Book> byPageNumber() {
        return book -> book.getNumberOfPages() == getPageNumber();
    }

}
