package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Task 4.: Find the books with maximum number of pages;
 */
public class LongestBookExample extends AbstractBookByPageNameExample {

    private static final String FORMAT = "The longest (%d pages) books: %s";

    public LongestBookExample(final Set<Author> authors, final List<Book> books) {
        super(authors, books);
    }

    @Override
    protected int getPageNumber() {
        return books.stream()
                .mapToInt(Book::getNumberOfPages)
                .max()
                .getAsInt();

    }

    @Override
    public String getResult() {
        return String.format(FORMAT, getPageNumber(), getBooksByPageNumber());
    }

}
