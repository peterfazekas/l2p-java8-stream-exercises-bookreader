package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;
import com.epam.bookreader.service.Console;

import java.util.*;
import java.util.function.Predicate;

/**
 * Task 4.: Find the books with maximum number of pages;
 */
public class LongestBookByAuthorExample extends AbstractExample {

    private static final String FORMAT = "    The longest book of author %s is '%s' (%d pages long)";
    private static final String NO_SUCH_BOOK_MESSAGE = "    Sorry, no such book exist.";
    private final String description;
    private final Console console;

    public LongestBookByAuthorExample(final Set<Author> authors, final List<Book> books, final String description) {
        super(authors, books);
        this.description = description;
        console = new Console(new Scanner(System.in));
    }


    @Override
    public String getResult() {
        String authorName = console.read(description);
        Optional<Book> longestBookByAuthor = getLogestBookByAuthor(authorName);
        return  longestBookByAuthor.isPresent()
                ? String.format(FORMAT, authorName, longestBookByAuthor.get().getTitle(), longestBookByAuthor.get().getNumberOfPages())
                : NO_SUCH_BOOK_MESSAGE;
    }

    private Optional<Book> getLogestBookByAuthor(final String author) {
        return books.stream()
                .filter(bookByAuthor(author))
                .sorted(Comparator.comparing(Book::getNumberOfPages).reversed())
                .findFirst();
    }

    private Predicate<Book> bookByAuthor(final String author) {
        return book -> book.isAuthor(author);
    }

}
