package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Task 7.: Get distinct list of all authors;
 */
public class DistinctAuthorsStreamExample extends AbstractExample {

    private static final String AUTHOR_FORMAT = "%n    - %s";
    private static final String FORMAT = "Distinct list of Authors: %s";

    public DistinctAuthorsStreamExample(final Set<Author> authors, final List<Book> books) {
        super(authors, books);
    }

    @Override
    public String getResult() {
        return String.format(FORMAT, getDistinctAuthors());
    }

    private String getDistinctAuthors() {
        return authors.stream()
                .distinct()
                .map(this::getAuthor)
                .collect(Collectors.joining());
    }

    private String getAuthor(final Author author) {
        return String.format(AUTHOR_FORMAT, author);
    }



}
