package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Set;

/**
 * Task 2.: Create a stream of books
 */
public class InitStreamExample extends AbstractExample {

    private static final String FORMAT = "\'Author\' DTO was initialized with %d items and \'Book\' DTO was initialized with %d items.";
    private static final String ERROR = "Something went wrong during DTO initialization";

    public InitStreamExample(final Set<Author> authors, final List<Book> books) {
        super(authors, books);
    }

    @Override
    public String getResult() {
        return isInitHappened() ? String.format(FORMAT, authors.size(), books.size()) : ERROR;
    }

    private boolean isInitHappened() {
        return books.size() > 0 && authors.size() > 0;
    }
}
