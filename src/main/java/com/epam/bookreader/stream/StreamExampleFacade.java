package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Set;

public class StreamExampleFacade {

    private final Set<Author> authors;
    private final List<Book> books;
    private AbstractExample example;

    public StreamExampleFacade(final Set<Author> authors, final List<Book> books) {
        this.authors = authors;
        this.books = books;
    }

    public String getInitState() {
        example = new InitStreamExample(authors, books);
        return example.getResult();
    }

    public String getBookByPageNumber(final int limit) {
        TimeMeasure example = new TimeMeasure(new CheckBookByPageNumberExample(authors, books, limit));
        return example.getResult();
    }

    public String getBookByPageNumberParallel(final int limit) {
        TimeMeasure example = new TimeMeasure(new CheckBookByPageNumberParallelExample(authors, books, limit));
        return example.getResult();
    }

    public String getLongestBook() {
        example = new LongestBookExample(authors, books);
        return example.getResult();
    }

    public String getShortestBook() {
        example = new ShortestBookExample(authors, books);
        return example.getResult();
    }

    public String getSingleAuthorBooks() {
        example = new SingleAuthorBooksStreamExample(authors, books);
        return example.getResult();
    }

    public String getAuthorsList() {
        example = new DistinctAuthorsStreamExample(authors, books);
        return example.getResult();
    }

    public String debugAuthorsList() {
        example = new DebugDistinctAuthorsStreamExample(authors, books);
        return example.getResult();
    }

    public String getLongestBookByAuthor(final String description) {
        example = new LongestBookByAuthorExample(authors, books, description);
        return example.getResult();
    }

}
