package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Task 5.: Filter books with only single author; sort the books by number of pages/title; get list of all titles;
 * Task 6.: Print them using forEach method
 */
public class SingleAuthorBooksStreamExample extends AbstractExample {

    private static final String FORMAT = "Filter books with only single author; sort the books by number of pages/title";

    public SingleAuthorBooksStreamExample(final Set<Author> authors, final List<Book> books) {
        super(authors, books);
    }

    @Override
    public String getResult() {
        StringBuilder sb = new StringBuilder(FORMAT);
        getTitleOfBooksBySingleAuthor().forEach(sb::append);
        return sb.toString();
    }

    private List<String> getTitleOfBooksBySingleAuthor() {
        return books.stream()
                .filter(Book::hasSingleAuthor)
                .sorted(Comparator.comparing(Book::getNumberOfPages).thenComparing(Book::getTitle))
                .map(Book::toString)
                .collect(Collectors.toList());
    }

}
