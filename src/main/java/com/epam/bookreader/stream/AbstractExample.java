package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Set;

public abstract class AbstractExample {

    protected final Set<Author> authors;
    protected final List<Book> books;

    protected AbstractExample(final Set<Author> authors, final List<Book> books) {
        this.authors = authors;
        this.books = books;
    }

    public abstract String getResult();

}
