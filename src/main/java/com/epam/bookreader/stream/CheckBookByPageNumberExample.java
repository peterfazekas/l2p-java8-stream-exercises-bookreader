package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.function.Predicate;

/**
 * Task 3.: Check if some/all book(s) have more than [LIMIT] pages;
 */
public class CheckBookByPageNumberExample extends AbstractExample {

    private static final String FORMAT = "%d out of %d books has more than %d pages.";
    private final int limit;

    public CheckBookByPageNumberExample(final Set<Author> authors, final List<Book> books, final int limit) {
        super(authors, books);
        this.limit = limit;
    }

    @Override
    public String getResult() {
        return String.format(FORMAT, checkBookByPageNumber(), books.size(), limit);
    }

    private long checkBookByPageNumber() {
        return books.stream()
                .filter(byPageNumberIsGreaterThanLimit())
                .count();
    }

    private Predicate<Book> byPageNumberIsGreaterThanLimit() {
        return book -> book.getNumberOfPages() > limit;
    }

}
