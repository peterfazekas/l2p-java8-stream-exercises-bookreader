package com.epam.bookreader.stream;

public class TimeMeasure {

    final private AbstractExample example;

    public TimeMeasure(final AbstractExample example) {
        this.example = example;
    }

    public String getResult() {
        long startTime = System.nanoTime();
        String functionResult = example.getResult();
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        return String.format("%s%n    Execution took: %d ns", functionResult, duration);
    }
}
