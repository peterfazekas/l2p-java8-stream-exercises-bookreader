package com.epam.bookreader.stream;

import com.epam.bookreader.domain.Author;
import com.epam.bookreader.domain.Book;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Task 8.: Use peek method for debugging intermediate streams during execution the previous task (Get distinct list of all authors).
 */
public class DebugDistinctAuthorsStreamExample extends AbstractExample {

    private static final String AUTHOR_FORMAT = "%n    - %s";
    private static final String FORMAT = "Distinct list of Authors: %s";
    private final StringBuilder debug1;
    private final StringBuilder debug2;
    private final StringBuilder debug3;

    public DebugDistinctAuthorsStreamExample(final Set<Author> authors, final List<Book> books) {
        super(authors, books);
        debug1 = new StringBuilder();
        debug2 = new StringBuilder();
        debug3 = new StringBuilder();
    }

    @Override
    public String getResult() {
        return String.format(FORMAT, getDistinctAuthors());
    }

    private String getDistinctAuthors() {
        return authors.stream()
                .peek(debug1::append)
                .distinct()
                .peek(debug2::append)
                .map(this::getAuthor)
                .peek(debug3::append)
                .collect(Collectors.joining());
    }

    private String getAuthor(final Author author) {
        return String.format(AUTHOR_FORMAT, author);
    }

}
