package com.epam.bookreader.domain;

import java.util.ArrayList;
import java.util.List;

public class Author implements Comparable<Author> {
    final String name;
    final List<Book>books;

    public Author(final String name) {
        this.name = name;
        books = new ArrayList<>();
    }

    public void setBooks(final List<Book> books) {
        this.books.addAll(books);
    }

    public String getName() {
        return name;
    }

    public List<Book> getBooks() {
        return books;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Author other) {
        return this.name.compareTo(other.name);
    }
}
