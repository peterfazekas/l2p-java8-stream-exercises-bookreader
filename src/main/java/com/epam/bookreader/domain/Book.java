package com.epam.bookreader.domain;

import java.util.List;

public class Book {

    private static final String BOOK_TITLE_FORMAT = "%n   - %s (%d pages)";

    final String title;
    final List<Author> authors;
    final int numberOfPages;

    public Book(final String title, final List<Author> authors, final int numberOfPages) {
        this.title = title;
        this.authors = authors;
        this.numberOfPages = numberOfPages;
    }

    public String getTitle() {
        return title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public boolean isAuthor(final Author author) {
        return authors.stream()
                .map(Author::getName)
                .anyMatch(i -> i.equals(author.getName()));
    }

    public boolean isAuthor(final String author) {
        return authors.stream()
                .map(Author::getName)
                .map(String::toLowerCase)
                .anyMatch(i -> i.equals(author));
    }

    public boolean hasSingleAuthor() {
        return authors.size() == 1;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    @Override
    public String toString() {
        return String.format(BOOK_TITLE_FORMAT, title, numberOfPages);
    }
}
